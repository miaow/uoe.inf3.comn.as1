#! /bin/bash

sleepTime=1;
img_input=../cwk_testfile.jpg
img_output=../test.jpg

echo "-------------------------";
echo "------STEP 2-------------";

ipfw -q flush
ipfw add pipe 6000 in > /dev/null
ipfw add pipe 6001 out > /dev/null
ipfw pipe 6000 config delay 10ms plr 0.05 bw 10Mbits/s > /dev/null
ipfw pipe 6001 config delay 10ms plr 0.05 bw 10Mbits/s > /dev/null

for timeout in 10 20 30 40 50 60 70 80 90 100; do
    echo "Timeout $timeout"

    java Receiver2 6001 $img_output &
    recPid=$!
    java Sender2 localhost 6001 $img_input $timeout &
    sendPid=$!

    wait $recPid
    sleep $sleepTime
    if kill -s 0 $sendPid 2> /dev/null; then
        echo "Sender is still alive... Killing"
        kill $sendPid
    fi

    diff $img_input $img_output

    echo
done

optimalTimeout=50
if [ "$optimalTimeout" = "Not Set" ]; then
    echo "You need to set an optimal timeout"
    exit 1
fi

echo "-------------------------";
echo "------STEP 3-------------";


for delay in 10 100 500; do
    ms="ms"
    ipfw -q flush
    ipfw add pipe 6000 in > /dev/null
    ipfw add pipe 6001 out > /dev/null
    ipfw pipe 6000 config delay $delay$ms plr 0.005 bw 10Mbits/s > /dev/null
    ipfw pipe 6001 config delay $delay$ms plr 0.005 bw 10Mbits/s > /dev/null

    for windowSize in 1 2 4 8 16 32 64 128 256; do
        echo "Delay $delay Window Size: $windowSize"
        java Receiver3 6001 $img_output &
        recPid=$!
        java Sender3 localhost 6001 $img_input $optimalTimeout $windowSize &
        sendPid=$!

        wait $recPid
        sleep $sleepTime
        if kill -s 0 $sendPid 2> /dev/null; then
            echo "Sender is still alive... Killing"
            kill $sendPid
        fi

        diff $img_input $img_output
    done

done

echo "-------------------------";
echo "------STEP 4-------------";

ipfw -q flush
ipfw add pipe 6000 in > /dev/null
ipfw add pipe 6001 out > /dev/null
ipfw pipe 6000 config delay 100ms plr 0.005 bw 10Mbits/s > /dev/null
ipfw pipe 6001 config delay 100ms plr 0.005 bw 10Mbits/s > /dev/null
for windowSize in 8 16 32 64 128 256; do
    echo "WindowSize: $windowSize"
    java Receiver4 6001 $img_output $windowSize &
    recPid=$!
    java Sender4 localhost 6001 $img_input $optimalTimeout $windowSize &
    sendPid=$!

    wait $recPid
    sleep $sleepTime
    if kill -s 0 $sendPid 2> /dev/null; then
        echo "Sender is still alive... Killing"
        kill $sendPid
    fi

    diff $img_input $img_output
done
