/* Mihail Atanassov s1018353 */

/*
 * Class Sender1. This is the sender, i.e. the client. The receiver should be 
 * up and running before this one executes.
 */

import java.io.*;
import java.net.*;

public class Sender1
{
	/*-- constants --*/
	static final int STRIDE 	= 1024; // Actual data - 1KiB packet
	static final int SEQ 		= 2; // 16-bit sequence num size
	static final int FLAGS_SIZE 	= 1; // 8-bit flags (only EOF is used)
	static final int HEADER 	= SEQ + FLAGS_SIZE;
	static final int PACKETSIZE 	= STRIDE + HEADER;
	static final int EOF_MASK   	= 0x1;
	
	/*-- members --*/
	private static byte[] data = null;
	private static boolean lastPacket = false;
	private static int port = -1;
	private static InetAddress host = null;

	public static void main(String[] args) throws IOException
	{
		/*-- Get the command line stuff --*/
		processCommandLine(args);

		/*-- Open up a UDP port --*/
		DatagramSocket senderSocket = null;
		try
		{
			senderSocket = new DatagramSocket();
		}
		catch (IOException e)
		{
			System.out.println("Failed to open socket");
			System.exit(-1);
		}
		
		/*
		 * index 	- the current index into the data array
		 * sequence 	- the serial number of the packet to be sent
		 * flags 	- the flags byte from the UDP packet. Bit 0 is
		 * 	the EOF flag.
		 */
		int index = 0;
		int sequence = 0;
		byte flags = 0;

		/* -- START Send loop --*/
		while ((flags & EOF_MASK) == 0) /* last packet check */
		{
			/* Check if we are at the last packet */
			if( index + STRIDE >= data.length )
			{
				flags |= EOF_MASK;
				lastPacket = true;
			}
			
			/* Generate full data section */
			byte[] sendData = new byte[PACKETSIZE];
			sendData[0] = (byte) ((sequence & 0xFF00) >> 8);
			sendData[1] = (byte) (sequence & 0xFF);
			sendData[2] = flags;
			System.arraycopy(data, index, 
					sendData, HEADER, 
					lastPacket ? 
					data.length - index : STRIDE);
			
			/* ... and send it over the network */
			DatagramPacket sendPkt = new DatagramPacket(sendData,
					PACKETSIZE, host, port);
			//System.out.println("packet out: " + sequence);
			senderSocket.send(sendPkt);

			/* Prepare for sending the next packet */
			++sequence;
			index += STRIDE;
			
			/* Wait for a while so as to not flood the network */
			try
			{
				Thread.sleep(20);
			}
			catch(InterruptedException e)
			{
				// do nothing
			}
		}
		/*-- END Send loop --*/
		System.out.println("File probably sent");
		senderSocket.close();
	}

	private static void processCommandLine(String[] args) throws IOException
	{
		// For this sender, we expect exactly 3 arguments. Anything
		// else and the program will exit
		if (args.length != 3)
		{
			System.out.println("Too many/few arguments");
			System.exit(-1);
		}
		
		// host
		host = InetAddress.getByName(args[0]);
		
		// port. Since there is no such thing in Java as an unsigned
		// short, I am using an int. What a waste of space...
		port = Integer.parseInt(args[1]);
		if (port > 65535 || port < 1024)
		{
			System.out.println("Port number too large/small");
			System.exit(-1);
		}
		
		// filename is in args[2]
		// NB: Assuming file does not change during our reading it
		java.io.RandomAccessFile file = null;
		try
		{
			file = new java.io.RandomAccessFile( args[2], "r" );
		}
		catch(FileNotFoundException e)
		{
			System.out.println("File not found");
			System.exit(-1);
		}
		long fileLength = file.length(); // the file length in bytes
		int fileLengthInt = (int) fileLength;
		data = new byte[fileLengthInt];
		try
		{
			if (fileLength != fileLengthInt)
				throw new IOException("File size too large");
			if (fileLength == 0L)
				throw new IOException("File does not exist");
			file.readFully(data);
		}
		finally
		{
			file.close();
		}
		
		System.out.println("host: " + host + "\nport: " + port
				+ "\nfile: " + args[2] + " : "
				+ fileLength + " B");
	}
}
