/* Mihail Atanassov s1018353 */

/*
 * STOP_AND_WAIT
 * Class Sender2. This is the sender, i.e. the client. The receiver should be 
 * up and running before this one executes.
 */
import java.io.*;
import java.net.*;
import java.util.Arrays;

//@formatter:off
public class Sender2
{

	/*-- constants --*/
	static final int STRIDE		= 1024;	// Actual data - 1KiB packet
	static final int SEQ		= 2;	// 16-bit sequence num size
	static final int FLAGS_SIZE	= 1;	// 8-bit flags (only EOF used)
	static final int HEADER		= SEQ + FLAGS_SIZE;
	static final int PACKETSIZE	= STRIDE + HEADER;
	static final int EOF_MASK	= 0x1;
	static final int ACKSIZE	= SEQ;
	
	/*-- members --*/
	private static byte[]		data			= null;
	private static boolean		lastPacket		= false;
	private static int		port			= -1;
	private static InetAddress	host			= null;
	private static int		retransmitTimeout	= -1;
	private static int		fileLengthInt		= -1;
	
//@formatter:on

	public static void main( String[] args ) throws IOException
	{
		/*-- Get the command line stuff --*/
		long time = System.currentTimeMillis();
		processCommandLine( args );

		/*-- Open up UDP port --*/
		DatagramSocket senderSocket = null;
		try
		{
			senderSocket = new DatagramSocket();
			senderSocket.setSoTimeout( retransmitTimeout );
		}
		catch( IOException e )
		{
			System.out.println( "Failed to open socket" );
			System.exit( -1 );
		}

		/*
		 * Stuff that will be needed to send the data 
		 * index - the current index into the data array 
		 * sequence - the serial number of the packet to be sent 
		 * flags - the flags byte from the UDP packet. Bit 0 is the EOF
		 * 	flag.
		 */
		int index = 0;
		int sequence = 0;
		int ackSequence;
		byte flags = 0;

		/*-- Pretty print --*/
		char[] symbols =
		{ '-', '\\', '|', '/' };
		int currSym = 0;

		/* -- START Send loop -- */
		int resendCount = 0;
		boolean sendingComplete = false;
		byte[] sendData = new byte[PACKETSIZE];
		byte[] ackData = new byte[ACKSIZE];
		System.out.printf( "Sending: [%c]\b\b", symbols[currSym] );
		while( !sendingComplete )
		{
			/* Check if we are at the last packet */
			if( index + STRIDE >= data.length )
			{
				flags |= EOF_MASK;
				lastPacket = true;
			}

			/* Generate full data section */
			Arrays.fill( sendData, ( byte ) 0 );
			sendData[0] = ( byte ) ( ( sequence & 0xFF00 ) >> 8 );
			sendData[1] = ( byte ) ( sequence & 0xFF );
			sendData[2] = flags;
			System.arraycopy( data, index, sendData, HEADER,
					lastPacket ? data.length - index
							: STRIDE );

			/* ... and send it over the network */
			DatagramPacket sendPkt = new DatagramPacket( sendData,
					PACKETSIZE, host, port );
			senderSocket.send( sendPkt );

			/* print rotating thingy */
			System.out.printf( "%c\b",
					symbols[( ( ++currSym ) % 4 )] );

			/* now wait for the ack for some time */
			DatagramPacket ackPkt = new DatagramPacket( ackData,
					ackData.length );

			try
			{
				senderSocket.receive( ackPkt );
			}
			catch( SocketTimeoutException e )
			{
				// we did not receive the ACK, so re-send the
				// packet
				++resendCount;
				continue;
			}
			//@formatter:off
			ackSequence = 	( 0xFF00 & 
					( ( int ) ackPkt.getData()[0] << 8 ) )
					+ ( 0xFF & 
					( ( int ) ackPkt.getData()[1] ) );
			//@formatter:on
			if( ackSequence != sequence )
			{
				// ACK mismatch. Re-send
				++resendCount;
				continue;
			}

			/* Are we at the last received packet? */
			if( lastPacket )
			{
				sendingComplete = true;
			}

			/* Prepare for sending the next packet */
			++sequence;
			index += STRIDE;

		}
		/*-- END Send loop --*/
		time = System.currentTimeMillis() - time;
		System.out.printf( "\rFile sent in %ds\nNumber of resends: %d\nAvg thruput: %6.2f KiB/s\n", time / 1000, resendCount, (float) fileLengthInt/time);
		senderSocket.close();
	}

	private static void processCommandLine( String[] args )
			throws IOException
	{
		// For this sender, we expect exactly 4 arguments. Anything
		// else and the program will exit
		if( args.length != 4 )
		{
			System.out.println( "Too many/few arguments" );
			System.exit( -1 );
		}

		// host
		host = InetAddress.getByName( args[0] );

		// port. Since there is no such thing in Java as an unsigned
		// short, I am using an int. What a waste of space...
		port = Integer.parseInt( args[1] );
		if( port > 65535 || port < 1024 )
		{
			System.out.println( "Port number too large/small" );
			System.exit( -1 );
		}

		// filename is in args[2]
		// NB: Assuming file does not change during our reading it
		java.io.RandomAccessFile file = null;
		try
		{
			file = new java.io.RandomAccessFile( args[2], "r" );
		}
		catch( FileNotFoundException e )
		{
			System.out.println( "File not found" );
			System.exit( -1 );
		}
		long fileLength = file.length(); // the file length in bytes
		fileLengthInt = ( int ) fileLength;
		data = new byte[fileLengthInt];
		try
		{
			if( fileLength != fileLengthInt )
			{
				throw new IOException( "File size too large" );
			}
			if( fileLength == 0L )
			{
				throw new IOException( "File does not exist" );
			}
			file.readFully( data );
		}
		finally
		{
			file.close();
		}

		// retransmit timeout is in args[3]
		retransmitTimeout = Integer.parseInt( args[3] );
		if( retransmitTimeout == -1 )
		{
			System.out.println( "What's this timeout?" );
			System.exit( -1 );
		}

		System.out.println( "host: " + host + "\nport: " + port
				+ "\nfile: " + args[2] + " : " + fileLength
				+ " B" );
	}
}
