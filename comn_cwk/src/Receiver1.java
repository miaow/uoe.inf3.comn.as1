/* Mihail Atanassov s1018353 */

/*
 * Class Receiver1. This is the receiver, i.e. the listen server. Command line
 * arguments are as specified in the coursework description. After receiving the
 * last packet, this server automatically shuts down.
 * NB: Make sure you have write access to the target file.
 */
import java.io.*;
import java.net.*;

public class Receiver1
{
	/*-- constants --*/
	static final int STRIDE = 1024; // Actual data - 1KiB packet
	static final int SEQ = 2; // 16-bit sequence num size
	static final int FLAGS_SIZE = 1; // 8-bit flags (only EOF is used)
	static final int HEADER = SEQ + FLAGS_SIZE;
	static final int PACKETSIZE = STRIDE + HEADER;
	static final int EOF_MASK = 0x1;

	/*-- Member variables --*/
	private static int port = -1;
	private static java.io.RandomAccessFile file = null;
	private static boolean lastPacket = false;
	private static int fileLength = 0;
	private static DatagramPacket rcvPacket = null;

	public static void main(String[] args) throws IOException
	{
		/*-- Get the command line stuff --*/
		processCommandLine(args);

		/*-- Open up UDP port and listen for data --*/
		DatagramSocket rcvSocket = null;
		try
		{
			rcvSocket = new DatagramSocket(port);
		}
		catch (IOException e)
		{
			System.out.println("Failed to open port " + port);
			System.exit(-1);
		}

		/*
		 * Structures that will hold arriving data
		 * byte[] receiveData - the full byte array from the UDP packet
		 * int sequenceRcv - the above, converted
		 * int sequenceExp - the expected sequence number. In this 
		 * 	part, this is just for a sanity check.
		 * byte flags - the flags byte from the UDP packet. Bit 0 is
		 * 	the EOF flag.
		 */
		byte[] receiveData = new byte[PACKETSIZE];
		int sequenceRcv = 0;
		int sequenceExp = 0;
		byte flags = 0;

		/*-- Begin waiting loop --*/
		rcvPacket = new DatagramPacket(
			receiveData, receiveData.length);

		while (!lastPacket)
		{
			/* 
			 * This suspends the process until a packet arrives.
			 * Handy when 100% CPU is undesirable.
			 */
			rcvSocket.receive(rcvPacket);

			/*-- START Packet handling --*/
			/* First, get the sequence number */
			sequenceRcv =
			(0xFF00 & ((int) rcvPacket.getData()[0] << 8))
			+ (0xFF & ((int) rcvPacket.getData()[1]));

			if (sequenceRcv != sequenceExp)
			{
				System.out.println("Sequence numbers don't "
						   + "match. Rerun both applications");
				System.exit(-1);
			}

			/* Next, check the flags */
			flags = rcvPacket.getData()[2];
			if ((flags & EOF_MASK) != 0)
			{
				lastPacket = true;
			}

			/* Finally, put the data into the file */
			writeToFile();
			/*-- END Packet handling --*/

			/* Increment the expected sequence number */
			++sequenceExp;
		}
		/* Last packet has arrived successfully */
		System.out.println("Successfully received file of length "
				   + fileLength);
		file.close();
	}

	/**
	 * Handles the command line input
	 *
	 * @param args the command line arguments
	 * @throws IOException
	 */
	private static void processCommandLine(String[] args) throws IOException
	{
		// For this receiver, we expect exactly 2 arguments. Anything
		// else and the program will exit
		if (args.length != 2)
		{
			System.out.println("Too many/few arguments");
			System.exit(-1);
		}
		// port. Since there is no such thing in Java as an unsigned
		// short, I am using an int. What a waste of space
		port = Integer.parseInt(args[0]);
		if (port > 65535 || port < 1024)
		{
			System.out.println("Port number too large/small\n"
					   + "Please use ports in the range"
					   + "[1024; 65535]");
			System.exit(-1);
		}

		// filename is in args[1]. Open it for writing
		try
		{
			file = new java.io.RandomAccessFile(args[1], "rw");
			file.setLength(0); // Delete any existing data
		}
		catch (SecurityException e)
		{
			System.out.println("You don't have write permission "
					   + "for that file");
			System.exit(-1);
		}
	}

	/**
	 * writes to the file given at the command line
	 *
	 * @throws IOException
	 */
	private static void writeToFile() throws IOException
	{
		if (!lastPacket)
		{
			file.write(rcvPacket.getData(), HEADER, STRIDE);
			fileLength += STRIDE;
		}
		else
		{
			/*
			 * This is the last packet. Since the buffer is
			 * larger (or equal) to the data size, traverse
			 * the array backwards until a data byte is
			 * found. That will give the final piece size
			 */
			int index = STRIDE;
			while (rcvPacket.getData()[--index + HEADER] == 0)
			{
				// Do nothing
			}
			/*
			 * the +1 negates the last pre-decrement of the
			 * while loop
			 */
			file.write(rcvPacket.getData(), HEADER, index + 1);
			fileLength += index + 1;
		}
	}
}
