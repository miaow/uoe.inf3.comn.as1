/* Mihail Atanassov s1018353 */

/*
 * STOP_AND_WAIT
 * Class Receiver2. This is the receiver, i.e. the listen server. Command line
 * arguments are as specified in the coursework description. After receiving the
 * last packet, this server automatically shuts down.
 * NB: Make sure you have write access to the target file.
 */
import java.io.*;
import java.net.*;

public class Receiver2
{

	/*-- constants --*/
	static final int STRIDE		= 1024; // Actual data - 1KiB packet
	static final int SEQ		= 2;	// 16-bit sequence num size
	static final int FLAGS_SIZE	= 1;	// 8-bit flags (only EOF used)
	static final int HEADER		= SEQ + FLAGS_SIZE;
	static final int PACKETSIZE	= STRIDE + HEADER;
	static final int EOF_MASK	= 0x1;
	static final int ACKSIZE	= SEQ; 	// we only need the sequence #
						// in the ACK

	/*-- Member variables --*/
	private static int port				= -1;
	private static java.io.RandomAccessFile file	= null;
	private static boolean lastPacket		= false;
	private static int fileLength			= 0;
	private static DatagramPacket rcvPacket		= null;
	private static DatagramPacket ackPacket		= null;

	public static void main(String[] args) throws IOException
	{
		/*-- Get the command line stuff --*/
		processCommandLine(args);

		/*-- Open up UDP port and listen for data --*/
		DatagramSocket rcvSocket = null;
		try
		{
			rcvSocket = new DatagramSocket(port);
		}
		catch (IOException e)
		{
			System.out.println("Failed to open port " + port);
			System.exit(-1);
		}

		/*
		 * Structures that will hold arriving data
		 * receiveData 	- the full byte array from the UDP packet
		 * ackData	- the UDP ACK byte array. 2 bytes wide
		 * sequenceRcv 	- the above, converted
		 * sequenceExp 	- the expected sequence number. In this 
		 * 	part, this is just for a sanity check.
		 * lastSequence - the last sequence number that has been 
		 * 	received.
		 * flags 	- the flags byte from the UDP packet. Bit 0 is
		 * 	the EOF flag.
		 */
		byte[] receiveData = new byte[PACKETSIZE];
		byte[] ackData = new byte[ACKSIZE];
		int sequenceRcv = 0;
		int sequenceExp = 0;
		int lastSequence = -1;
		byte flags = 0;

		/*-- Begin waiting loop --*/
		rcvPacket = new DatagramPacket(
			receiveData, receiveData.length);
		System.out.printf("Waiting for first packet...\r");
		while (!lastPacket)
		{
			/* 
			 * This suspends the process until a packet arrives.
			 * Handy when 100% CPU is undesirable.
			 */

			rcvSocket.receive(rcvPacket);

			/*-- START Packet handling --*/

			/* First, get the sequence number */
			sequenceRcv =
			(0xFF00 & ((int) rcvPacket.getData()[0] << 8))
			+ (0xFF & ((int) rcvPacket.getData()[1]));
			if (sequenceRcv != sequenceExp)
			{

				// resend the last successfully received
				// packet number
				ackData[0] = (byte) ((lastSequence & 0xFF00) >> 8);
				ackData[1] = (byte) (lastSequence & 0xFF);
			}
			else
			{
				/* we have the packet we wanted */
				ackData[0] = (byte) ((sequenceRcv & 0xFF00) >> 8);
				ackData[1] = (byte) (sequenceRcv & 0xFF);
				lastSequence = sequenceRcv;
				++sequenceExp;

				/* check the flags */
				flags = rcvPacket.getData()[2];
				if ((flags & EOF_MASK) != 0)
				{
					lastPacket = true;
				}

				/* put the data into the file */
				writeToFile();
			}

			/* ... and send the ack */
			ackPacket = new DatagramPacket(ackData,
						       ackData.length,
						       rcvPacket.getAddress(),
						       rcvPacket.getPort());
			rcvSocket.send(ackPacket);
			if(lastPacket == true)
			{
				// send some more acks just in case some of them
				// are lost in transit
				// 6 9's certainty on a 10% plr
				for(int i = 0; i <5; ++i)
					rcvSocket.send( ackPacket );
			}

			System.out.printf("\rReceived pkt: %d            ",
					  lastSequence);
			/*-- END Packet handling --*/
		}
		/* Last packet has arrived successfully */
		System.out.println("\rSuccessfully received file of length "
				   + fileLength);
		file.close();
	}

	private static void processCommandLine(String[] args) throws IOException
	{
		// For this receiver, we expect exactly 2 arguments. Anything
		// else and the program will exit
		if (args.length != 2)
		{
			System.out.println("Too many/few arguments");
			System.exit(-1);
		}
		// port. Since there is no such thing in Java as an unsigned
		// short, I am using an int. What a waste of space
		port = Integer.parseInt(args[0]);
		if (port > 65535 || port < 1024)
		{
			System.out.println("Port number too large/small\n"
					   + "Please use ports in the range"
					   + "[1024; 65535]");
			System.exit(-1);
		}

		// filename is in args[1]. Open it for writing
		try
		{
			file = new java.io.RandomAccessFile(args[1], "rw");
			file.setLength(0); // Delete any existing data
		}
		catch (SecurityException e)
		{
			System.out.println("You don't have write permission "
					   + "for that file");
			System.exit(-1);
		}
	}

	private static void writeToFile() throws IOException
	{
		if (!lastPacket)
		{
			file.write(rcvPacket.getData(), HEADER, STRIDE);
			fileLength += STRIDE;
		}
		else
		{
			/*
			 * This is the last packet. Since the buffer is
			 * larger (or equal) to the data size, traverse
			 * the array backwards until a data byte is
			 * found. That will give the final piece size
			 */
			int index = STRIDE;
			while (rcvPacket.getData()[--index + HEADER] == 0)
			{
				// Do nothing
			}
			/*
			 * the +1 negates the last pre-decrement of the
			 * while loop
			 */
			file.write(rcvPacket.getData(),
				   HEADER,
				   index + 1);
			fileLength += index + 1;
		}
	}
}
