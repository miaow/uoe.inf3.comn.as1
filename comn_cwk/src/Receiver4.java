/* Mihail Atanassov s1018353 */

/*
 * SELECTIVE_REPEAT
 * Class Receiver4. This is the receiver, i.e. the listen server. Command line
 * arguments are as specified in the coursework description. After receiving the
 * last packet, this server automatically shuts down.
 * NB: Make sure you have write access to the target file.
 */
import java.io.*;
import java.net.*;
import java.util.List;
import java.util.ArrayList;

public class Receiver4
{

	/*-- constants --*/
	static final int			STRIDE		= 1024;			// Actual data - 1KiB packet
	static final int			SEQ		= 2;				// 16-bit sequence num size
	static final int			FLAGS_SIZE	= 1;				// 8-bit flags (only EOF used)
	static final int			HEADER		= SEQ
										+ FLAGS_SIZE;
	static final int			PACKETSIZE	= STRIDE
										+ HEADER;
	static final int			EOF_MASK	= 0x1;
	static final int			ACKSIZE		= SEQ;				// we only need the sequence #
												// in the ACK

	/*-- Member variables --*/
	private static int			port		= -1;
	private static java.io.RandomAccessFile	file		= null;
	private static int			wndSize		= -1;
	private static boolean			lastPacket	= false;
	private static int			fileLength	= 0;
	private static DatagramPacket		rcvPacket	= null;
	private static DatagramPacket		ackPacket	= null;

	public static void main( String[] args ) throws IOException
	{
		/*-- Get the command line stuff --*/
		processCommandLine( args );

		/*-- Open up UDP port and listen for data --*/
		DatagramSocket rcvSocket = null;
		try
		{
			rcvSocket = new DatagramSocket( port );
		}
		catch( IOException e )
		{
			System.out.println( "Failed to open port " + port );
			System.exit( -1 );
		}

		/*
		 * Structures that will hold arriving data
		 * receiveData 	- the full byte array from the UDP packet
		 * ackData	- the UDP ACK byte array. 2 bytes wide
		 * sequenceRcv 	- the above, converted
		 * lastSequence - the last sequence number that has been 
		 * 	received.
		 * flags 	- the flags byte from the UDP packet. Bit 0 is
		 * 	the EOF flag.
		 */
		byte[] receiveData = new byte[PACKETSIZE];
		byte[] ackData = new byte[ACKSIZE];
		int seqRcv = 0;
		byte flags = 0;

		MessageList msgLst = new MessageList();

		/*-- Begin waiting loop --*/
		rcvPacket = new DatagramPacket( receiveData, receiveData.length );
		System.out.printf( "Waiting for first packet...\r" );
		while( !msgLst.allRcvd() )
		{
			/* 
			 * This suspends the process until a packet arrives.
			 * Handy when 100% CPU is undesirable.
			 */

			rcvSocket.receive( rcvPacket );

			/*-- START Packet handling --*/
			int wndBase = msgLst.getWndBase();

			/* First, get the sequence number */
			//@formatter:off
			seqRcv = 
			( 0xFF00 & ( ( int ) rcvPacket.getData()[0] << 8 ) )
			+ ( 0xFF & ( ( int ) rcvPacket.getData()[1] ) );
//			System.out.println("seq: " + seqRcv + " base: " + wndBase);
			//@formatter:on

			// if sequenceRcv < wndBase, we just ack 
			// and do nothing else
			// if sequenceRcv > wndBase + wndSize - 1, we got a prob
			if( seqRcv >= wndBase + wndSize )
			{
				System.out.println( "dafuq just happened?\n" +
						"make sure that sender and " +
						"receiver have the same " +
						"window sizes" );
				System.exit( -1 );
			}
			//			if (sequenceRcv != sequenceExp)
			//			{
			//
			//				// resend the last successfully received
			//				// packet number
			//				ackData[0] = (byte) ((lastSequence & 0xFF00) >> 8);
			//				ackData[1] = (byte) (lastSequence & 0xFF);
			//			}
			//			else
			//			{
			//				/* we have the packet we wanted */
			//				ackData[0] = (byte) ((sequenceRcv & 0xFF00) >> 8);
			//				ackData[1] = (byte) (sequenceRcv & 0xFF);
			//				lastSequence = sequenceRcv;
			//				++sequenceExp;
			//
			//				/* check the flags */
			//				flags = rcvPacket.getData()[2];
			//				if ((flags & EOF_MASK) != 0)
			//				{
			//					lastPacket = true;
			//				}
			//
			//				/* put the data into the file */
			//				writeToFile();
			//			}

			/* check the flags */
			flags = rcvPacket.getData()[2];
			if( ( flags & EOF_MASK ) != 0 )
			{
				lastPacket = true;
			}

			/* add the message to the list - handles duplicates 
			 * automatically */
			byte[] messageData = new byte[STRIDE];
			System.arraycopy( rcvPacket.getData(), 3, messageData,
					0, STRIDE );

			msgLst.addMsg( messageData, seqRcv, lastPacket );

			/* ... and send the ack */
			// always return the ack number of the packet just rcvd
			ackData[0] = rcvPacket.getData()[0];
			ackData[1] = rcvPacket.getData()[1];
			if( ackPacket == null )
				ackPacket = new DatagramPacket( ackData,
						ackData.length,
						rcvPacket.getAddress(),
						rcvPacket.getPort() );
			rcvSocket.send( ackPacket );

			System.out.printf( "\rReceived pkt: %d            ",
					seqRcv );
			/*-- END Packet handling --*/
		}
		/* Last packet has arrived successfully */

		/* if the last ACK was lost... */
		while( true )
		{
			try
			{
				/* if we don't get anything within a sec,
				 * then call it quits.
				 */
				rcvSocket.setSoTimeout( 1000 );
				rcvSocket.receive( rcvPacket );
			}
			catch( SocketTimeoutException e )
			{
				break;
			}

			// return the lost last ack
			ackData[0] = rcvPacket.getData()[0];
			ackData[1] = rcvPacket.getData()[1];
			if( ackPacket == null )
				ackPacket = new DatagramPacket( ackData,
						ackData.length,
						rcvPacket.getAddress(),
						rcvPacket.getPort() );
			rcvSocket.send( ackPacket );
			for( int i = 0; i < 9; ++i )
			{
				rcvSocket.send( ackPacket ); //do it more times
				// just in case...
			}
		}

		msgLst.writeToFile( file );
		System.out.println( "\rSuccessfully received file of length "
				+ fileLength );
		file.close();
	}

	private static void processCommandLine( String[] args )
			throws IOException
	{
		// For this receiver, we expect exactly 3 arguments. Anything
		// else and the program will exit
		if( args.length != 3 )
		{
			System.out.println( "Too many/few arguments" );
			System.exit( -1 );
		}
		// port. Since there is no such thing in Java as an unsigned
		// short, I am using an int. What a waste of space
		port = Integer.parseInt( args[0] );
		if( port > 65535 || port < 1024 )
		{
			System.out.println( "Port number too large/small\n"
					+ "Please use ports in the range"
					+ "[1024; 65535]" );
			System.exit( -1 );
		}

		// filename is in args[1]. Open it for writing
		try
		{
			file = new java.io.RandomAccessFile( args[1], "rw" );
			file.setLength( 0 ); // Delete any existing data
		}
		catch( SecurityException e )
		{
			System.out.println( "You don't have write permission "
					+ "for that file" );
			System.exit( -1 );
		}

		// window size in args[2]
		wndSize = Integer.parseInt( args[2] );
	}

	private static class MessageList
	{

		private List<Message>	msgLst;

		public MessageList()
		{
			msgLst = new ArrayList<Message>();
		}

		public void addMsg( byte[] data_, int seq_, boolean last_ )
		{
			if( seq_ == msgLst.size() )
			{
				// sequence number is in order, so just add
				msgLst.add( new Message( data_, last_ ) );
			}
			else if( seq_ > msgLst.size() )
			{
				// we need to add placeholder messages
				for( int i = msgLst.size(); i < seq_; ++i )
				{
					msgLst.add( new Message( null, false ) );
				}
				msgLst.add( new Message( data_, last_ ) );
			}
			else
			{
				// we need to update existing invalid entry
				// or do nothing with the duplicate pkt
				if( msgLst.get( seq_ ).valid )
					// we have a duplicate pkt - do nothing
					return;
				msgLst.get( seq_ ).validate( data_ );
			}
		}

		public boolean allRcvd()
		{
			if( msgLst.size() == 0)
			{
				return false;
			}
			if( msgLst.get( msgLst.size() - 1 ).last == false )
				return false;
			else
			{
				for( Message m : msgLst )
				{
					if( m.valid == false )
						return false;
				}
			}
			return true;
		}

		public int getWndBase()
		{
			for( int i = 0; i < msgLst.size(); ++i )
			{
//				System.out.println("message " + i + " valid: " + msgLst.get( i ).valid);
				if( i == msgLst.size() - 1 )
					if (msgLst.get(i).valid)
						return msgLst.size();
				
				if( msgLst.get( i ).valid == false )
				{
					return i;
				}
			}
			return 0;
		}

		public void writeToFile( java.io.RandomAccessFile file_ )
				throws IOException
		{
			//@formatter:off
			/* for each packet */
			for( int i = 0; i < msgLst.size(); ++i)
			{
				// write it to file
				if (i != msgLst.size() - 1)
				{
					// not the last pkt
					file.write( msgLst.get( i ).data, 
							0, 
							STRIDE );
					fileLength += STRIDE;
				}
				else
				{
					/*
					 * This is the last packet. Since the 
					 * buffer is larger (or equal) to the 
					 * data size, traverse the array 
					 * backwards until a data byte is found.
					 * That will give the final piece size.
					 */
					int index = STRIDE;
					while( msgLst.get( i )
						.data[--index] == 0 )
					{
						// Do nothing
					}
					/*
					 * the +1 negates the last pre-decrement
					 * of the while loop
					 */
					file.write( msgLst.get( i ).data, 
							0, index + 1 );
					fileLength += index + 1;
				}
			}
			//@formatter:on
		}
	}

	private static class Message
	{
		public byte[]	data;
		public boolean	valid;
		public boolean	last;

		public Message( byte[] data_, boolean last_ )
		{
			data = data_;
			valid = ( data == null ) ? false : true;
			last = last_;
		}

		public void validate( byte[] data_ )
		{
			if( !valid )
			{
				data = data_;
				if( data == null )
					return;
				valid = true;
			}
			return;
		}
	}
}
