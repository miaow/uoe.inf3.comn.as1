/* Mihail Atanassov s1018353 */

/*
 * GO_BACK_N
 * Class Sender3. This is the sender, i.e. the client. The receiver should be 
 * up and running before this one executes.
 */

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

//@formatter:off
public class Sender3
{
	/*-- constants --*/
	static final int  STRIDE 	= 1024;	// Actual data - 1KiB packet
	static final int  SEQ 		= 2;	// 16-bit sequence num size
	static final int  FLAGS_SIZE 	= 1;	// 8-bit flags (only EOF used)
	static final int  HEADER 	= SEQ + FLAGS_SIZE;
	static final int  PACKETSIZE 	= STRIDE + HEADER;
	static final byte EOF_MASK 	= 0x1;
	static final int  ACKSIZE 	= SEQ;

	/*-- members --*/
	private static int		port 			= 0;
	private static InetAddress	host			= null;
	private static int		retransmitTimeout 	= 0;
	private static int		windowSize		= 0;
	private static DataHandler	dataHandler		= null;
	private static MessageList	msgList			= null;
	private static ACKHandler	ackHnd			= null;
	private static Semaphore	sem			= null;

//@formatter:on

	private static void processCommandLine( String[] args )
	{
		// For this sender, we expect exactly 5 arguments. Anything
		// else and the program will exit
		if( args.length != 5 )
		{
			System.out.println( "Too many/few arguments" );
			System.exit( -1 );
		}

		// --1-- host
		try
		{
			host = InetAddress.getByName( args[0] );
		}
		catch( UnknownHostException e )
		{
			System.out.println( "Unknown host" );
			System.exit( -1 );
		}

		// --2-- port. Since there is no such thing in Java as an 
		// unsigned short, I am using an int. What a waste of space...
		port = Integer.parseInt( args[1] );
		if( port > 65535 || port < 1024 )
		{
			System.out.println( "Port number too large/small" );
			System.exit( -1 );
		}

		// --3-- filename is in args[2]
		try
		{
			dataHandler = new DataHandler( args[2] );
			int sequence = 0;
			msgList = new MessageList();
			while( dataHandler.hasMoreData() )
			{
				msgList.add( sequence++,
						dataHandler.getNextChunk(),
						!dataHandler.hasMoreData() );
			}
		}
		catch( IOException e )
		{
			System.out.println( "Failed to init data handler" );
			System.exit( -1 );
		}

		// --4-- retransmit timeout is in args[3]
		retransmitTimeout = Integer.parseInt( args[3] );
		if( retransmitTimeout <= 0 )
		{
			System.out.println( "Invalid timeout value." );
			System.exit( -1 );
		}

		// --5-- window size
		windowSize = Integer.parseInt( args[4] );
		if( windowSize <= 0 )
		{
			System.out.println( "Window size invalid." );
			System.exit( -1 );
		}

		System.out.println( "host: " + host + "\nport: " + port
				+ "\nfile: " + args[2] + " : "
				+ dataHandler.fileLength + " B" );
	} /* END private static void processCommandLine( String[] args ) */

	public static void main( String[] args )
	{
		long time = System.currentTimeMillis();

		/*-- Get the command line stuff --*/
		processCommandLine( args );

		/*-- Open up UDP port --*/
		DatagramSocket senderSocket = null;
		try
		{
			senderSocket = new DatagramSocket();
			senderSocket.setSoTimeout( retransmitTimeout );
		}
		catch( IOException e )
		{
			System.out.println( "Failed to open socket" );
			System.exit( -1 );
		}

		/*-- fire up the semaphore --*/
		sem = new Semaphore( 1 );
		
		/*-- Pretty print --*/
		char[] symbols =
		{ '-', '\\', '|', '/' };
		int currSym = 0;

		/*-- Set up the ACK Handler --*/
		ackHnd = new ACKHandler( msgList, senderSocket );
		Thread ackHndThread = new Thread( ackHnd );
		ackHndThread.start();

		/* -- START Send loop -- */
		System.out.printf( "Sending: [%c]\b\b", symbols[currSym] );
		int resendCount = 0;
		int windowBase = 0;
		while( !msgList.allRcvd() )
		{
			/* print rotating thingy */
			System.out.printf( "%c\b",
					symbols[( ( ++currSym ) % 4 )] );

			/* move the window base to its proper position */
			windowBase = msgList.getWndBase();

			int windowEnd = windowBase + windowSize -1 >= msgList
					.length() ? msgList.length() - 1
					: windowBase + windowSize - 1;
			/* for each packet in the window:
			 * if it is not sent, send it.
			 * if lost, re-send it.
			 * if sent or received, do nothing
			 */
			//@formatter:off
			try
			{
				sem.acquire();
			}
			catch(InterruptedException e)
			{
				// do nothing
			}
			
			for( 	int i = windowBase; 
				i <= windowEnd; 
				++i )
			//@formatter:on
			{
				switch( msgList.getState( i ) )
				{

					case LOST:
						++resendCount;
					case UNST:
						msgList.send( i, senderSocket );
						break;
					case SENT:
					case RCVD:
					default:
						break;
				}
			}
			sem.release();
		}
		//@formatter:off
		/*-- END Send loop --*/
		// The ACK handler has stopped on its own by now
		time = System.currentTimeMillis() - time;
		System.out.printf(
	"\rFile sent in %ds\nNumber of resends: %d\nAvg thruput: %6.2f KiB/s\n",
				time / 1000, resendCount, 
				(float)dataHandler.fileLengthInt/time );
		senderSocket.close();
		//@formatter:on
	} /* END public static void Main( String[] args ) */

	/**
	 * The ACK listener class - this will wait for the ACKs that should
	 * arrive eventually. Also writes to file
	 */
	private static class ACKHandler implements Runnable
	{
		private byte[]		ackData;
		private DatagramPacket	ackPacket;
		private MessageList	msgLst;
		private DatagramSocket	socket;
		private long		timeNow;

		/**
		 * Ctor; creates a packet for sending over the net.
		 * 
		 * @param messageList_
		 *                the message list containing all the packet
		 *                data
		 * @param socket_
		 *                the socket through which to send
		 */
		public ACKHandler( MessageList messageList_,
				DatagramSocket socket_ )
		{
			ackData = new byte[ACKSIZE];
			ackPacket = new DatagramPacket( ackData, ACKSIZE );
			msgLst = messageList_;
			socket = socket_;
		}

		/**
		 * The main loop of the secondary thread. This will wait for
		 * ACKs until all messages have been received.
		 */
		@Override
		public void run()
		{
			while( !msgLst.allRcvd() )
			{
				//@formatter:off
				int windowBase = msgLst.getWndBase();
				int windowEnd = ( windowBase + windowSize -1 >= 
						msgLst.length() ) ? 
						msgLst.length() - 1 : 
						windowBase + windowSize -1;
				try
				{
					sem.acquire();
				}
				catch(InterruptedException e)
				{
					//do nothing
				}
				for( int i = windowBase; i <= windowEnd; ++i )
				{
					getAck(i);
				}
				sem.release();
				//@formatter:on
			}
		}

		/**
		 * Waits for an ACK until the message's retransmit timeout is
		 * reached
		 * 
		 * @param i
		 *                the sequence number of the message
		 */
		private void getAck( int i )
		{
			try
			{
				if( msgLst.isSent( i ) )
				{
					//@formatter:off
					timeNow = System.currentTimeMillis();
					long diff = 
						retransmitTimeout - 
						( timeNow - 
						msgLst.getDispatchTime( i ) );
					
					if( diff <= 0 )
						socket.setSoTimeout( 1 );
					else
						socket.setSoTimeout( 
							( int ) ( diff ) );

					socket.receive( ackPacket );
					// We've got an ACK, signal that this
					// message is in
					int seq = ((((int) 
					ackPacket.getData()[0]) & 0xFF ) << 8)
					+ ((int) ackPacket.getData()[1] & 0xFF);
					msgLst.messageRcvd( seq, true );
					// consider the message lost just in
					// case (if the rcvd ack is not the one
					// we expected). If we rcv a later ack,
					// just move our expectation. Only works
					// on GBN, not on SEL_RPT
					if(seq < i)
						msgLst.messageLost( i );
					else
						i = seq;
					//@formatter:on
				}
				else
				{
					return;
				}

			}
			catch( SocketTimeoutException e )
			{
				// Socket timed out - wait for next ACK
				msgLst.messageLost( i );
				return;
			}
			catch( IOException e )
			{
				// Could not access socket
//				e.printStackTrace();
				System.exit( -1 );
			}
		}
	}

	/**
	 * The class that handles the input data (in the form of a binary file)
	 */
	private static class DataHandler
	{
		private byte[]		data;
		private int		index;
		public final long	fileLength;
		public final int	fileLengthInt;

		/**
		 * Ctor; reads the input file and stores it in a byte[].
		 * 
		 * @param filename
		 *                the input file name
		 * @throws IOException
		 *                 if the file size is over 2GiB or if it does
		 *                 not exist
		 */
		public DataHandler( String filename ) throws IOException
		{
			index = 0;
			// NB: Assuming file does not change during our read op
			java.io.RandomAccessFile file = null;
			try
			{
				file = new java.io.RandomAccessFile( filename,
						"r" );
			}
			catch( FileNotFoundException e )
			{
				throw new IOException( "File not found" );
			}
			fileLength = file.length(); // the file length in bytes
			fileLengthInt = ( int ) fileLength;
			try
			{
				if( fileLength != fileLengthInt )
				{
					throw new IOException(
							"File size too large" );
				}
				if( fileLength == 0L )
				{
					throw new IOException(
							"File does not exist" );
				}
				data = new byte[fileLengthInt];
				file.readFully( data );
			}
			finally
			{
				file.close();
			}
		}

		/**
		 * @return are we at the end of the file?
		 */
		public boolean hasMoreData()
		{
			return index < data.length;
		}

		/**
		 * @return the 1KiB chunk of data in the sequence. If the chunk
		 *         is the last one (and smaller than 1KiB), it is padded
		 *         to fill the 1KiB array.
		 */
		public byte[] getNextChunk()
		{
			byte[] chunk = new byte[STRIDE];
			//@formatter:off
			System.arraycopy(
					data, index,
					chunk, 0,
					index + STRIDE >= data.length ? 
							data.length
							- index
							: STRIDE );
			//@formatter:on
			index += STRIDE;
			return chunk;
		}
	}

	/**
	 * The list of messages that will be sent over the network.
	 */
	private static class MessageList
	{
		private List<Message>	messageList;

		/**
		 * Constructor; creates the message list
		 */
		public MessageList()
		{
			messageList = new ArrayList<Message>();
		}
		
		/**
		 * @return the number of messages in the list
		 */
		public synchronized int length()
		{
			return messageList.size();
		}

		/**
		 * Sends the message with the given sequence number through the
		 * given socket
		 * 
		 * @param seq
		 *                the sequence number
		 * @param socket
		 *                the socket
		 */
		public synchronized void send( int seq, DatagramSocket socket )
		{
			messageList.get( seq ).send( socket );
		}

		/**
		 * @param seq
		 *                the message sequence number
		 * @return whether the packet has been sent but unACKed
		 */
		public synchronized boolean isSent( int seq )
		{
			return messageList.get( seq ).state == PacketState.SENT;
		}

		/**
		 * @return whether all packets have been ack'ed by the receiver
		 */
		public boolean allRcvd()
		{
			for( Message m : messageList )
			{
				if( m.state != PacketState.RCVD )
					return false;
			}
			return true;
		}

		/**
		 * @param seq
		 *                the message sequence number
		 * @return the state of the message
		 */
		public PacketState getState( int seq )
		{
			return messageList.get( seq ).state;
		}

		/**
		 * @param seq
		 *                the message sequence number
		 * @return the time at which the packet was last dispatched. If
		 *         the packet is unsent, returns -1
		 */
		public synchronized final long getDispatchTime( int seq )
		{
			return messageList.get( seq ).dispatchTime;
		}

		/**
		 * @return the first packet which has not been acknowledged,
		 *         i.e. the base of the sender window.
		 */
		private int wndBase = 0;
		public synchronized int getWndBase()
		{
			//@formatter:off
//			int sequence = 0;
			for(int i = wndBase; i < messageList.size(); ++i)
			{
				if ( messageList.get(i).state == 
						PacketState.RCVD)
					++wndBase;
				else
					break;
			}
			return wndBase;
			//@formatter:on
		}

		/**
		 * Adds a new message to the list
		 * 
		 * @param s_
		 *                the sequence number of the message
		 * @param p_
		 *                the payload of the message
		 * @param l_
		 *                whether that message is the last one
		 * @return true if operation succeeded, false otherwise
		 */
		public synchronized boolean add( int s_, byte[] p_, boolean l_ )
		{
			return messageList.add( new Message( s_, p_, l_ ) );
		}

		/**
		 * Changes the state for message number seq to LOST.
		 * 
		 * @param seq
		 *                the message number
		 */
		public synchronized void messageLost( int seq )
		{
			messageList.get( seq ).state = PacketState.LOST;
		}

		/**
		 * Changes the state for message number seq to RCVD.
		 * 
		 * @param seq
		 *                the message number
		 */
		public synchronized void messageRcvd( int seq,
				boolean cumulative )
		{
			if( cumulative )
			{
				for( int i = getWndBase(); i <= seq; ++i )
				{
					messageList.get( i ).state = 
							PacketState.RCVD;
				}
			}
			else
			{
				messageList.get( seq ).state = PacketState.RCVD;
			}
		}
	}

	/**
	 * Holds a single packet that can be sent over the link. Has a sequence
	 * number, state, and dispatch time in addition to the data.
	 */
	private static class Message
	{
		public final int	sequence;
		private byte[]		msgData;
		private long		dispatchTime;
		private PacketState	state;
		DatagramPacket		packet;

		/**
		 * Creates a new message
		 * 
		 * @param sequence_
		 *                the sequence number of the packet
		 * @param payload_
		 *                the actual data that will be transmitted
		 * @param lastMessage_
		 *                whether that is the final packet
		 */
		public Message( int sequence_, byte[] payload_,
				boolean lastMessage_ )
		{
			sequence = sequence_;
			msgData = new byte[PACKETSIZE];
			System.arraycopy( payload_, 0, msgData, 3,
					payload_.length );
			dispatchTime = -1;
			state = PacketState.UNST;

			msgData[0] = ( byte ) ( ( sequence & 0xFF00 ) >> 8 );
			msgData[1] = ( byte ) ( sequence & 0xFF );
			msgData[2] = lastMessage_ ? ( byte ) EOF_MASK
					: ( byte ) 0;

			packet = new DatagramPacket( msgData, msgData.length,
					host, port );
		}

		/**
		 * Sends the packet through a given socket.
		 * 
		 * @param socket
		 *                the socket
		 */
		private void send( DatagramSocket socket )
		{
			try
			{
				socket.send( packet );
				dispatchTime = System.currentTimeMillis();
				state = PacketState.SENT;
			}
			catch( IOException e )
			{
				System.out.println( "Failed to send" );
			}
		}
	}

	//@formatter:off
	/**
	 * Enum for the state that each packet can be in. 
	 * UNST: unsent 
	 * SENT: sent (i.e. still within the timeout period) 
	 * RCVD: received (i.e. ACK arrived successfully) 
	 * LOST: lost (i.e. ACK didn't arrive within the timeout period)
	 */
	//@formatter:on
	private static enum PacketState
	{
		UNST, SENT, RCVD, LOST
	}
}
