COMN Coursework Sources
=======================

Sender1.java and Receiver1.java
-------------------------------
The bare-bones client-server pair. No error detection, no recovery from a lost packet.

Sender2.java and Receiver2.java
-------------------------------
Client-server pair utilising a Stop-and-Wait [SAW] protocol.

Sender3.java and Receiver3.java
-------------------------------
Go-Back-N [GBN] protocol.

Sender4.java and Receiver4.java
-------------------------------
Selective Repeat [SACK] protocol.
